/*! All Rights Reserved. Copyright 2012 (C) TOYOTA  MOTOR  CORPORATION.
Last Update: 2016/08/19 */

/**
 * file service.js<br />
 *
 * @fileoverview このファイルには、閲覧画面(CD版)についての処理が<br />
 * 定義されています。<br />
 * file-> service.js
 * @author 渡会
 * @version 1.1.0
 *
 * History(date|version|name|desc)<br />
 *  2011/03/09|1.0.0   |渡会|新規作成<br />
 *  2016/08/19|1.1.0   |今泉|REL_00292<br />
 */
/*-----------------------------------------------------------------------------
 * サービス情報高度化プロジェクト
 * 更新履歴
 * 2011/03/09 渡会 ・新規作成
 *---------------------------------------------------------------------------*/
/**
 * 閲覧画面クラス
 * @namespace 閲覧画面クラス
 */
var Service = {};

/**
 * 隠し項目一覧
 * @type object(連想配列)
 */
Service.globalInfo  = {
  VIEW_LANG:          "",
  PUB_BIND_ID:        "",
  FOR_LANG:           "",
  BRAND:              "",
  BRAND_NAME:         "",
  CAR_NAME:           "",
  TYPE:               "",
  OPTION1:            "",
  OPTION2:            "",
  OPTION3:            "",
  OPTION4:            "",
  OPTION5:            "",
  TEKI_DATE:          "",
  LANG_CODE:          "",
  LANG_NAME:          "",
  CAR_TYPE:           "",
  MANUALS:            "",
  START_TYPE:         "",
  SEARCH_TYPE:        "",
  FROM_DATE:          "",
  MODEL_YEAR:         "",
  PARTS_CD:           "",
  DEFF_MANUALS:       "",
  SCH_OPT_DEF:        "",
  SCH_OPT_SEL:        "",
  SCH_OPT_RES:        "",
  SCH_OPT_INF:        "",
  CONTENT_TYPE:       "",
  SEARCH_TYPE:        "0",
  REPAIR_CONTENTS_TYPE_GROUPS:  "10",
  NCF_CONTENTS_TYPE_GROUPS:  "10",
  BRM_CONTENTS_TYPE_GROUPS:  "",
  KEYWORD:            null,
  SYSTEM_TYPE:        "0"
};

/**
 * 現在のウィンドウサイズ
 * @private
 * @type number
 */
Service.currHeight = null;

/**
 * フェイスボックスインスタンス
 * @private
 * @type Facebox
 */
Service.faceBox = null;

/**
 * 配線図プロパティ
 * @private
 * @type object(連想配列)
 */
Service.ewdProps = {
  "processType" : "",
  "linkKey"     : "",
  "functionId"  : ""
};

/**
 * 閲覧画面クラスの初期化処理
 */
Service.$init = function() {
  var METHODNAME = "Service.$init";
  try {
    
    var ewd = null;
    
    Service.faceBox = new Facebox();
    
    Use.$init(Service.faceBox);
    Use.Util.$stopSubmit(document.forms[0]);
    
    // GlobalInfo取得
    Service.$setGlobalInfo();
    
    /* 追加 */
    ewd = Service.EWD.$getInstance();
    ewd.init(Service.$getGlobalInfo());
    if(document.location.search != "") {
        ewd.show(Service.$getGlobalInfo(), "1", document.location.search.substr(1));
    } else {
        ewd.show(Service.$getGlobalInfo(), "1", "0");
    }
    // コンテンツ表示エリアのリサイズイベント登録
    Service.$setWindowResizeEvent();
    
  } catch(err) {
    Use.SystemError.$show(err, METHODNAME);
  }
};

/**
 * グローバルインフォの設定処理
 * @private
 */
Service.$setGlobalInfo = function() {
  var METHODNAME = "Service.$setGlobalInfo";
  try {
    
    var owner = window.opener;
    var myMap = Service.$getGlobalInfo();
    var parentGi = owner.Contents.$setGlobalInfo();
    
    myMap.VIEW_LANG   = parentGi.VIEW_LANG;
    myMap.PUB_BIND_ID = parentGi.PUB_BIND_ID;
    myMap.FOR_LANG    = parentGi.FOR_LANG;
    myMap.BRAND       = parentGi.BRAND;
    myMap.BRAND_NAME  = parentGi.BRAND;
    myMap.OPTION1     = parentGi.OPTION1;
    myMap.OPTION2     = parentGi.OPTION2;
    myMap.OPTION3     = parentGi.OPTION3;
    myMap.OPTION4     = parentGi.OPTION4;
    myMap.OPTION5     = parentGi.OPTION5;
    myMap.PARTS_CD    = parentGi.PARTS_CD;
    myMap.LANG_CODE   = parentGi.LANG_CODE;
    myMap.LANG_NAME   = parentGi.LANG_NAME;
    myMap.START_TYPE  = parentGi.START_TYPE;
    myMap.CAR_NAME    = parentGi.CAR_NAME;
    myMap.CAR_TYPE    = parentGi.CAR_TYPE;
    myMap.TYPE        = parentGi.TYPE;
    myMap.KEYWORD     = parentGi.KEYWORD;
    myMap.MANUALS     = parentGi.MANUALS;
    myMap.FROM_DATE   = parentGi.FROM_DATE;
    myMap.MODEL_YEAR  = parentGi.MODEL_YEAR;
    myMap.TEKI_DATE   = parentGi.TEKI_DATE;
    myMap.SCH_OPT_DEF = parentGi.SCH_OPT_DEF;
    myMap.SCH_OPT_SEL = parentGi.SCH_OPT_SEL;
    myMap.SCH_OPT_INF = parentGi.SCH_OPT_INF;
    myMap.SYSTEM_TYPE = Use.SYSTEM_TYPE;
    myMap.DEFF_MANUALS = [];
    myMap.CONTENT_TYPE = parentGi.CONTENT_TYPE;
    myMap.RM_LINK_FLAG = parentGi.RM_LINK_FLAG;
    
    Service.instancePartsCode = myMap.PARTS_CD;
    
    // ブランドが空白以外の場合、変換する
    if(myMap.BRAND != "") {
      myMap.BRAND = DictConst.C_BRAND_CODE[myMap.BRAND];
    }
    
    Service.globalInfo = myMap;
    
    Use.Util.$setViewLang(myMap.VIEW_LANG);
    
  } catch(err) {
    Use.SystemError.$show(err, METHODNAME);
  }
};

/**
 * グローバルインフォの取得処理
 * @return {object(連想配列)} グローバルインフォ
 */
Service.$getGlobalInfo = function() {
  var METHODNAME = "Service.$getGlobalInfo";
  try {
    
    var giCopy = {};
    
    Util.$propcopy(Service.globalInfo, giCopy);
    
    return giCopy;
    
  } catch(err) {
    Use.SystemError.$show(err, METHODNAME);
  }
};

/**
 * コンテンツ表示エリアのイベント設定処理
 * @private
 * @param {document} owner この画面の親画面DOM情報
 */
Service.$setWindowResizeEvent = function(owner) {
  var METHODNAME = "Service.$setWindowResizeEvent";
  try {
    
    /* @private */
    var myFunc  = function() {
      var targets = $$("div.tab_body");
      var len     = targets.length;
      var myStyle = { height: "" };
      var elmH = 0;
      var curH = Util.$getClientHeight(true);
      var rate = 0;
      var winSize = "";
      var winHeight = "";
      
      // 現在の画面サイズが未取得の場合は取得
      if(Service.currHeight === null) {
        winSize = Util.$createWindowOption("",1);
        winHeight = winSize.split(",");
        Service.currHeight = winHeight[1].substring(7);
        // 表示領域のサイズからタブの高さを取得
        //elmH = Service.currHeight - 175;
        //単体ビュワー対応
        elmH = Service.currHeight - 81;
        // 配線図
        myStyle.height = elmH + "px";
        Element.$setStyle($('tab_body_ewd'), myStyle);
        Element.$redraw($("footer"));
      }
      
      rate = curH - Service.currHeight;
      
      // rateが0以外の場合はサイズ変更処理を行う
      if(rate) {
        // サイズ変更の対象エレメント数だけループする
        for(var i = 0; i < len; i++) {
          elmH = (!Util.$isUndefined(targets[i]._currHeight)) ?
              targets[i]._currHeight : parseInt(
              Element.$getStyle(targets[i], "height").replace("px", ""), 10);
          // 対象エレメントのスタイルがある場合は取得高さ+差分を、無い場合は
          // 0を設定する
          elmH = !isNaN(elmH) ? elmH + rate : 0;
          // 対象エレメントのサイズを隠し属性で保持
          targets[i]._currHeight = elmH;
          // 0未満になった場合は0にする
          if(elmH < 0) {
            elmH = 0;
          }
          myStyle.height = elmH + "px";
          Element.$setStyle(targets[i], myStyle);
          Element.$redraw(targets[i]);
        }
        Service.currHeight = curH;
        Element.$redraw($("footer"));
      }
    };
    
    Use.Util.$observe(window, "load", function() {
      Use.Util.$observe(window, "resize", myFunc);
      Use.Util.$delay(function(){Event.$fireEvent(window, 'resize');}, 0.1);
    });
    
  } catch(err) {
    Use.SystemError.$show(err, METHODNAME);
  }
};

// 配線図要求API対応

/**
 * 
 */
Service.$getProcessType = function() {
  var METHODNAME = "Service.$getProcessType";
  try {
    
    return Service.ewdProps.processType;
    
  } catch(err) {
    Use.SystemError.$show(err, METHODNAME, "", true);
  }
};

/**
 * リンクキー取得処理
 * @return string リンクキー
 */
Service.$getLinkKey = function() {
  var METHODNAME = "Service.$getLinkKey";
  try {
    
    return Service.ewdProps.linkKey;
    
  } catch(err) {
    Use.SystemError.$show(err, METHODNAME, "", true);
  }
};

/**
 * 配線図プロパティのセッター
 * @param {string} linkKey リンクキー
 * @param {string} process 処理区分
 */
Service.$setEWDProperty = function(linkKey, process) {
  var METHODNAME = "Service.$setEWDProperty";
  try {
    
    Service.ewdProps.linkKey = linkKey || "";
    Service.ewdProps.processType = process || "";
    
  } catch(err) {
    Use.SystemError.$show(err, METHODNAME);
  }
}

/**
 * 閲覧ログ登録処理
 * @param {string} funcId 機能ID
 */
Service.$entryAccessLog = function(funcId) {
  var METHODNAME = "Service.$entryAccessLog";
  try {
    
    
    
  } catch(err) {
    Use.SystemError.$show(err, METHODNAME, "", true);
  }
};

/**
 * システム区分取得処理
 * @return number システムタイプ(1) 1:CD版 2:Web版
 */
Service.$getSystemType = function() {
  var METHODNAME = "Service.$getSystemType";
  try {
    
    return Service.globalInfo.SYSTEM_TYPE;
    
  } catch(err) {
    Use.SystemError.$show(err, METHODNAME, "", true);
  }
};

/**
 * 配線図用 ツールアイコンエリア制御処理
 * @param {string} icnType 対象アイコン "1":印刷、"2":診断メモ
 * @param {boolean} isState 対象アイコンの状態制御 true:活性、false:非活性
 */
Service.$setToolIconEnable = function(icnType, isState) {
  var METHODNAME = "Service.$setToolIconEnable";
  try {
    
    var icnTypeMap = {
      "1" : "ewd_print",
      "2" : "ewd_memo"
    };
    var befElm = icnTypeMap[icnType];
    var aftElm = icnTypeMap[icnType];
    
    // trueの時は活性制御を行う
    if(isState) {
      befElm += "_g";
    // falseの時は非活性制御を行う
    } else {
      aftElm += "_g";
    }
    
    Element.$addClassName(befElm, "invisible");
    Element.$removeClassName(aftElm, "invisible");
    
  } catch(err) {
    Use.SystemError.$show(err, METHODNAME);
  }
};

